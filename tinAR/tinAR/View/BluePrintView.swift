//
//  BluePrintView.swift
//  tinAR
//
//  Created by Nauman on 08/01/2018.
//  Copyright © 2018 Khunshan Ahmad. All rights reserved.
//

import Foundation
import UIKit
import GLKit
import SceneKit

class BluePrintView:UIView {
    
    var nodesGroups:[NodesGroup]?
    var layoutRect:CGRect?
    var containerViewRect:CGRect?
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {return }
        
        if let _ = nodesGroups {
            
            for nodeGroup in nodesGroups! {
                drawNodeGroup(nodeGroup: nodeGroup, context: context)
            }
        }
    }
    
    //Draw Node Group
    private func drawNodeGroup(nodeGroup: NodesGroup, context: CGContext) {
        
        if nodeGroup.nodesGroupType == .room || nodeGroup.nodesGroupType == .window {
            drawRoomOrWindow(nodeGroup: nodeGroup, context: context)
        }
        else if nodeGroup.nodesGroupType == .door {
            drawDoor(nodeGroup: nodeGroup, context: context)
        }
        
    }
    
    //Helper
    private func drawDoor(nodeGroup: NodesGroup, context: CGContext) {
        
        let points:[CGPoint] = nodeGroup.allSphereNodes.map({
            let p = CGPoint(x: CGFloat($0.position.x), y: CGFloat($0.position.z))
            let convertedPoint = convert(p, from: layoutRect!, to: containerViewRect!)
            return convertedPoint
        })
        
        // Draw them with a 2.0 stroke width so they are a bit more visible.
        context.setLineWidth(2.0)

        let point1 = points.first!
        let point2 = points.last!
        
        let deltaX = point1.x - point2.x
        let deltaY = point1.y - point2.y
        
        let length = sqrt(deltaX * deltaX + deltaY * deltaY)
        
        //Rotating at theta angle from point1
        let aTheta = (atan2((point2.y - point1.y), (point2.x - point1.x)))
        let nTheta = CGFloat(GLKMathDegreesToRadians(45))
        
        let theta = aTheta + nTheta
        
        let x = (length * cos(theta))//(point1.y * sin(theta))
        let y = (length * sin(theta))// + (point1.x * sin(theta))
        //x=xcosθ−ysinθ
        //y=ycosθ+xsinθ
       
        let point3 = CGPoint(x: point1.x + x, y: point1.y + y)

        context.move(to: point1)
        context.addLine(to: point2)
        context.move(to: point1)
        context.addLine(to: point3)
        
        let color = nodeGroup.nodesGroupColor
        context.setStrokeColor(color.cgColor)
        
        // and now draw the Path
        context.strokePath()
        
        //Arc
        let dy = point3.y - point2.y
        let dx = point3.x - point2.x
        
        let n = CGPoint(x: dy,y: -dx)
        
        let w:CGFloat = 12
        let n1 = dy/magnitude(ofPoint: n)
        let n2 = -dx/magnitude(ofPoint: n)

        let weightedN = CGPoint(x: n1 * w, y: n2 * w)
        
        let mid = midPoint(a: point3, b: point2)
        
        let controlPoint = CGPoint(x: mid.x + weightedN.x , y: mid.y + weightedN.y)
        
        let path = UIBezierPath()
        path.move(to: point2)
        path.addQuadCurve(to: point3,controlPoint: controlPoint)
        path.lineWidth = (2.0)
        color.setStroke()
        path.stroke()
        
        createDistanceNodes(context: context, nodeGroup: nodeGroup, points: points)
    }
    
    private func drawRoomOrWindow(nodeGroup: NodesGroup, context: CGContext) {
        
        let points:[CGPoint] = nodeGroup.allSphereNodes.map({
            let p = CGPoint(x: CGFloat($0.position.x), y: CGFloat($0.position.z))
            let convertedPoint = convert(p, from: layoutRect!, to: containerViewRect!)
            return convertedPoint
        })
        
        // Draw them with a 2.0 stroke width so they are a bit more visible.
        context.setLineWidth(2.0);
        context.move(to: points.first!)
        context.addLines(between: points)
        
        let c = nodeGroup.nodesGroupColor
        context.setStrokeColor(c.cgColor)
        
        // and now draw the Path
        context.strokePath();
        
        createDistanceNodes(context: context, nodeGroup: nodeGroup, points: points)
    }
    
    private func createDistanceNodes(context: CGContext?, nodeGroup: NodesGroup, points: [CGPoint]) {
        //Text drawing
        var distancesList = [""]
        
        var previousNode: SCNNode?
        nodeGroup.allSphereNodes.forEach { (currentNode) in
            if previousNode != nil {
                let previousPos = previousNode?.position
                let currentPos = currentNode.position
                
                let distance = distanceBetweenPoints(A: currentPos, B: previousPos!)
                let distanceString = String(format: "%.2f", distance) + "m"
                
                distancesList.append(distanceString)
            }
            
            previousNode = currentNode
        }
        
        drawDistances(context: context, distances: distancesList, points: points, type: nodeGroup.nodesGroupType)
    }
    
    private func drawDistances(context: CGContext?, distances distancesList: [String], points pointsList: [CGPoint], type: NodesGroupType) {
        
        for (index, distance) in distancesList.enumerated() {
            
            if !distance.isEmpty {
                
                
                var attributedString = NSAttributedString(string: distance)
                
                let pointA = pointsList[index]
                let pointB = pointsList[index-1]
                
                let point = midPoint(a: pointA, b: pointB)
                
                //If you have two points, (x0, y0) and (x1, y1), then the angle of the line joining them (relative to the X axis) is given by: theta = atan2((y1 - y0), (x1 - x0))
                var theta = atan2((pointB.y - pointA.y), (pointB.x - pointA.x))

                if type == NodesGroupType.door {
                    attributedString = NSAttributedString(string: "Door")
                    theta += CGFloat.pi
                }
                else if type == NodesGroupType.window {
                    attributedString = NSAttributedString(string: "Window")
                    theta += CGFloat.pi
                }
                
                print("theta : \(theta)")

                context!.saveGState()
            
                context!.textMatrix = .identity
                context!.translateBy(x: point.x, y: point.y)
                context!.rotate(by: theta)
                context!.translateBy(x: -point.x, y: -point.y)
                
                let xForText = point.x - (attributedString.size().width * 0.5)
                attributedString.draw(at: CGPoint(x:xForText , y:point.y))
                
                context!.restoreGState()
                
            }
        }
    }
    
    private func midPoint(a: CGPoint, b: CGPoint) -> CGPoint {
        var p:CGPoint = .zero
        p.x = (a.x+b.x)/2;
        p.y = (a.y+b.y)/2;
        return p;
    }
    
    private func convert(_ point: CGPoint, from fromRect: CGRect, to toRect: CGRect) -> CGPoint {
        /*
         x1 = fromRect
         x2 = toRect
         Rectangle 1 has (x1, y1) origin and (w1, h1) for width and height, and
         Rectangle 2 has (x2, y2) origin and (w2, h2) for width and height, then
         
         Given point (x, y) in terms of Rectangle 1 coords, to convert it to Rectangle 2 coords:
         
         xNew = ((x-x1)/w1)*w2 + x2;
         yNew = ((y-y1)/h1)*h2 + y2;
         */
        
        let x1 = fromRect.origin.x
        let y1 = fromRect.origin.y
        let w1 = fromRect.size.width
        let h1 = fromRect.size.height
        
        let x2 = toRect.origin.x
        let y2 = toRect.origin.y
        let w2 = toRect.size.width
        let h2 = toRect.size.height
        
        let x = point.x
        let y = point.y
        
        let scaleX = w2/w1
        let scaleY = h2/h1
        let scale = CGFloat.minimum(scaleX, scaleY)
        
        //let side1 = max(w1, h1)
        //let side2 = max(w2, h2)
        
        let xNew = (x-x1)*scale + x2
        let yNew = (y-y1)*scale + y2
        
        return CGPoint(x: xNew, y: yNew)
    }
}

