//
//  FocusNode.swift
//  tinAR
//
//  Created by Khunshan Ahmad on 1/9/18.
//  Copyright © 2018 Khunshan Ahmad. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class FocusNode: SCNNode {
    
    let L:CGFloat = 70
    let S:CGFloat = 3
    let color:UIColor = .red
    
    override init() {
        super.init()
        
        let vbar = SCNPlane(width: S, height: L)
        vbar.firstMaterial?.diffuse.contents = color
        vbar.firstMaterial?.lightingModel = .constant
        vbar.firstMaterial?.isDoubleSided = true
        
        let hbar = SCNPlane(width: L, height: S)
        hbar.firstMaterial?.diffuse.contents = color
        hbar.firstMaterial?.lightingModel = .constant
        hbar.firstMaterial?.isDoubleSided = true
    
        let vbarNode = SCNNode(geometry: vbar)
        vbarNode.position.z += 0.2
        
        let hbarNode = SCNNode(geometry: hbar)
        hbarNode.position.z += 0.1
        
        let bar = SCNNode()
        bar.addChildNode(hbarNode)
        bar.addChildNode(vbarNode)

        bar.opacity = 0.8
        
        addChildNode(bar)

        eulerAngles.x = -.pi / 2

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
