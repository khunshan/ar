//
//  NodesModel.swift
//  tinAR
//
//  Created by Khunshan Ahmad on 1/8/18.
//  Copyright © 2018 Khunshan Ahmad. All rights reserved.
//

import Foundation
import SceneKit

class NodesGroup {
    
    var nodesGroupType      = NodesGroupType.room
    var allSphereNodes      = [SCNNode]()
    var allLineNodes        = [SCNNode]()
    var allDistanceNodes    = [(node: SCNNode, distance: String)]()
    
    var nodesGroupColor: UIColor {
        var c:UIColor?
        
        switch nodesGroupType {
        case .room:
            c = .red
            break
        case .window:
            c = .yellow
            break
        case .door:
            c = .green
            break
        }
        return c!
    }
    
    init(groupType: NodesGroupType) {
        self.nodesGroupType = groupType
    }
    
}


