//
//  BlueprintViewController.swift
//  tinAR
//
//  Created by Khunshan Ahmad on 1/5/18.
//  Copyright © 2018 Khunshan Ahmad. All rights reserved.
//

import UIKit

protocol BlueprintProtocol: class {
    func totalNodes() -> [NodesGroup]
    func totalArea() -> CGRect
}

class BlueprintViewController: UIViewController {
    
    //Properties
    @IBOutlet var mainView: BluePrintView!
    
    var mainViewRect:CGRect {
        let x = mainView.frame.origin.x
        let y = mainView.frame.origin.y
        let containerRect = CGRect(x: x, y: y,
                                   width: mainView.frame.width - (x*2),
                                   height: mainView.frame.height - (y*2))
        return containerRect
    }
    
    weak var delegate: BlueprintProtocol?
    
    
    //View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        drawBlueprint()
    }
    
    @IBAction func dismiss(sender: Any) {
        self.dismiss(animated: true)
    }
    
    //Methods
    private func drawBlueprint() {
        
        guard let nodesGroups = delegate?.totalNodes() else { return }
        guard let layoutRect  = delegate?.totalArea() else { return }
        
        mainView.layoutRect = layoutRect
        mainView.containerViewRect = mainViewRect
        mainView.nodesGroups = nodesGroups
        
        mainView.setNeedsDisplay()

        print("done")
    }
    
    
    @IBAction func ShareBtnPressed() {
        exportImage()
    }

    private func exportImage() {
        UIGraphicsBeginImageContext(view.frame.size)
        mainView.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        let textToShare = "Here's the floor plan for my room!"
        
        let objectsToShare = [textToShare, image] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
}
