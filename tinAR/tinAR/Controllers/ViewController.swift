//
//  ViewController.swift
//  tinAR
//
//  Created by Khunshan Ahmad on 1/2/18.
//  Copyright © 2018 Khunshan Ahmad. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController {
    
    //Properties
    @IBOutlet var statusLabel       : UILabel!
    @IBOutlet var sceneView         : ARSCNView!
    @IBOutlet var centerImageView   : UIImageView!
    
    @IBOutlet var drawPlanButton    : UIButton!
    @IBOutlet var closeNodesButton  : UIButton!
    @IBOutlet var clearButton       : UIButton!
    
    var nodesGroups     = [NodesGroup]()
    var nodesGroup      : NodesGroup?
    var selectedType    : NodesGroupType = .room

    let coolColor       = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    var planeOpacity: CGFloat = PlaneOpacity.high
    
    var allPlanes        = [ARPlaneAnchor: (SCNPlane, SCNNode)]()
    var savedPlane       : (ARPlaneAnchor, SCNPlane, SCNNode)?
    
    var cameraNode       : SCNNode?
    var startNode        : SCNNode?
    var latestNode       : SCNNode?
    
    var currentNode      : FocusNode?
    var currentLineNode  : SCNNode?
    var currentTextNode  : SCNNode?
    var currentText      : SCNText?
    
    var isPlaneDetected : Bool?
    
    var maxX, minX, maxY, minY: CGFloat!

    var smallSphere: (sphere: SCNSphere, scale: SCNVector3) {
        
        let dot = SCNSphere(radius:2)
        dot.firstMaterial?.diffuse.contents = coolColor
        dot.firstMaterial?.lightingModel = .constant
        dot.firstMaterial?.isDoubleSided = true
        
        let scale = SCNVector3(1/400.0, 1/400.0, 1/400.0)
        
        return (dot, scale)
    }
    
    var sceneViewCenter: CGPoint {
        return CGPoint(x: self.sceneView.bounds.midX, y: self.sceneView.bounds.midY)
    }
    
    var childCameraNode:SCNNode?
    
    var sceneViewFarCenter: SCNVector3 {
        return childCameraNode!.worldPosition
    }
    
    
    
    
    
    
    //View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        sceneView.showsStatistics = false
        
        isPlaneDetected = false
        
        //let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        sceneView.scene = SCNScene() // scene
        sceneView.session.delegate = self
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
        cameraNode = SCNNode()
        sceneView.scene.rootNode.addChildNode(cameraNode!)
        
        centerImageView.tintColor = coolColor
        
        currentNode = FocusNode() //SCNNode(geometry: smallSphere.sphere)
        currentNode?.scale = smallSphere.scale
        currentNode?.isHidden = true
        self.sceneView?.scene.rootNode.addChildNode(currentNode!)
        
        let text = SCNText (string: "0 m", extrusionDepth: 0.1)
        text.font = UIFont.systemFont(ofSize: 20)
        text.firstMaterial?.diffuse.contents = coolColor
        text.alignmentMode  = kCAAlignmentCenter
        text.truncationMode = kCATruncationMiddle
        text.firstMaterial?.isDoubleSided = true
        
        let textNode = SCNNode(geometry: text)
        textNode.scale = SCNVector3(1/500.0, 1/500.0, 1/500.0)
        textNode.eulerAngles = SCNVector3Make(0, .pi, 0)
        
        let textWrapNode = SCNNode()
        textWrapNode.addChildNode(textNode)
        
        let constraint = SCNLookAtConstraint(target: cameraNode)
        constraint.isGimbalLockEnabled = true
        textWrapNode.constraints = [constraint]
        self.sceneView?.scene.rootNode.addChildNode(textWrapNode)
        
        currentTextNode = textWrapNode
        currentTextNode?.isHidden = true

        currentText = text

        changeAppState(state: .planeNotSaved)
        
        ///////
        self.childCameraNode = SCNNode(geometry: smallSphere.sphere)
        self.childCameraNode?.position = SCNVector3(x: 0, y:0, z:-10)
        self.childCameraNode?.scale = SCNVector3(1/50.0, 1/50.0, 1/50.0)
        sceneView.pointOfView?.addChildNode(childCameraNode!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        sceneView.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? BlueprintViewController {
            vc.delegate = self
        }
    }
    
    //Methods
    @objc
    private func handleTap(sender: UITapGestureRecognizer)  {
        print(#function)
        
        if isPlaneDetected! {
            hitTest(at: sceneViewCenter)
        }
        else {
            hitTest(at: sceneViewCenter, hitTestCase: HitTestCase.savePlane)
        }
    }
    
    private func hitTest(at point: CGPoint, hitTestCase: HitTestCase = .drawSphere) {
        
        //let hits = sceneView.hitTest(point, options: [SCNHitTestOption: Any]())
        let hits = sceneView.hitTest(point, types: [
            //.featurePoint,
            .existingPlane,
            //.estimatedHorizontalPlane,
            //.existingPlaneUsingExtent,
            ])
        
        //Debug Hit Test Begins
        /*print("AllPlanes count: \(allPlanes.count)")
        print("hits count: \( hits.count)")
        allPlanes.keys.forEach({ print("allPlanes uuid: " + $0.identifier.uuidString) })
        hits.forEach({ print("hits uuid: " + ($0.anchor?.identifier.uuidString)!) })*/
        //Debug Hit Test Ends
        
        var intersectPosition: SCNVector3? {
            
            print("intersect Position")
            
            var p:SCNVector3?
            
            if selectedType == NodesGroupType.room {
                if let hitToUse = hits.filter({ $0.anchor?.identifier.uuidString == savedPlane?.0.identifier.uuidString }).first {
                    p = positionFromTransform(hitToUse.worldTransform)
                }
            } else {
                let rayHit = sceneView.scene.physicsWorld.rayTestWithSegment(from: cameraNode!.position, to: sceneViewFarCenter, options: [SCNPhysicsWorld.TestOption.backfaceCulling:false]).first
                
                if rayHit != nil {
                    p = rayHit!.worldCoordinates
                }
            }
            
            return p
        }

        if hitTestCase == .savePlane, hits.count > 0 {
            
            let furtherstHit = hits.last
            let plane = furtherstHit?.anchor?.identifier.uuidString
            
            let fPlane = allPlanes.filter({
                $0.key.identifier.uuidString == plane
            }).first
            
            let furtherstPlane = fPlane!
            
            //(key: ARPlaneAnchor, value: (SCNPlane, SCNNode))
            self.savedPlane = (furtherstPlane.key, furtherstPlane.value.0, furtherstPlane.value.1)
            
            changeAppState(state: .planeSaved)
        }
            
        else if hitTestCase == .drawSphere, var intersectPosition = intersectPosition {
            
//            if selectedType != .room {
//                let (newHitTestPosition, selectedNodeGroup, selectedNodeIndex, _) = mapPointToLine(point: intersectPosition)
//                intersectPosition = newHitTestPosition
//                
//                let newNode = SCNNode(geometry: smallSphere.sphere)
//                newNode.scale = smallSphere.scale
//                newNode.position = newHitTestPosition
//                self.sceneView?.scene.rootNode.addChildNode(newNode)
//                
//                selectedNodeGroup.allSphereNodes.insert(newNode, at: selectedNodeIndex)
//            }
            
            addShapeNode(atPosition: intersectPosition)
        }
            
        else if hitTestCase == .drawFloatingSphere, savedPlane != nil, intersectPosition != nil {
            
            moveShapeNode(toPosition: intersectPosition!)
            
            if latestNode != nil {
                currentTextNode?.position = intersectPosition!
                let distance = distanceBetweenPoints(A: latestNode!.position, B: intersectPosition!)
                currentText?.string = String(format: "%.2f m", distance)
            }
        }
            
        else {
            print("No Hit there")
        }
        
    }
    
    private func mapPointToLine(point: SCNVector3) -> (SCNVector3, NodesGroup, Int, CGFloat){
        
        var maxCosAngle = -CGFloat.infinity
        
        var node1: SCNNode?
        var node2: SCNNode?
        var vector1: SCNVector3?
        
        var selectedNodeGroup: NodesGroup?
        var selectedNodeIndex = 0
        
        nodesGroups.forEach { (nodeGroup) in
            var previousNode: SCNNode?

            var index = 0
            
            nodeGroup.allSphereNodes.forEach{ (currentNode) in
                if previousNode != nil {
                    let p1 = previousNode!.position
                    let p2 = currentNode.position
                    
                    let v1 = SCNVector3Make(p1.x - point.x, p1.y - point.y, p1.z - point.z)
                    let v2 = SCNVector3Make(p2.x - point.x, p2.y - point.y, p2.z - point.z)
                    
                    let cosAngle = cosAngleBetweenVectors(vector1: v1, vector2: v2)
                    
                    var angle = acos(cosAngle)
                    angle = angle.truncatingRemainder(dividingBy: CGFloat.pi)
                    
                    if angle > maxCosAngle {
                        node1 = previousNode
                        node2 = currentNode
                        vector1 = v1
                        
                        selectedNodeGroup = nodeGroup
                        selectedNodeIndex = index
                        maxCosAngle = angle
                    }
                }
                
                previousNode = currentNode
                index = index + 1
            }
        }
        
        var differenceVector = SCNVector3Make(node2!.position.x - node1!.position.x, node2!.position.y - node1!.position.y, node2!.position.z - node1!.position.z)
        
        let distanceRatio = dotProductBetweenVectors(vector1: vector1!, vector2: differenceVector) / dotProductBetweenVectors(vector1: differenceVector, vector2: differenceVector)
        
        differenceVector = SCNVector3Make(Float(distanceRatio * CGFloat(differenceVector.x)), Float(distanceRatio * CGFloat(differenceVector.y)), Float(distanceRatio * CGFloat(differenceVector.z)))
        
        let newPosition = SCNVector3Make(node1!.position.x - differenceVector.x, node1!.position.y - differenceVector.y, node1!.position.z - differenceVector.z)
        
        let distance = magnitude(ofVector: differenceVector)
        
        return (newPosition, selectedNodeGroup!, selectedNodeIndex, distance)
    }
    
    private func addShapeNode(atPosition planeHitTestPosition: SCNVector3) {
        
        //print("addShopeNode @ -> \(planeHitTestPosition)")
        
        //Update min and max coordinates of the scene which can be used later on to determine the overall bounding box of the room layout...
        maxX = CGFloat.maximum(maxX, CGFloat(planeHitTestPosition.x));
        minX = CGFloat.minimum(minX, CGFloat(planeHitTestPosition.x));
        
        maxY = CGFloat.maximum(maxY, CGFloat(planeHitTestPosition.z));
        minY = CGFloat.minimum(minY, CGFloat(planeHitTestPosition.z));
        
        if startNode == nil {
            createNodesGroup(ofType: selectedType)
            startNode = SCNNode(geometry: smallSphere.sphere)
            startNode?.scale = smallSphere.scale
            startNode?.position = planeHitTestPosition
            self.sceneView?.scene.rootNode.addChildNode(startNode!)
            nodesGroup?.allSphereNodes.append(startNode!)
            latestNode = startNode
            
            currentTextNode?.isHidden = false;
        }
        else {
            
            let newNode = SCNNode(geometry: smallSphere.sphere)
            newNode.scale = smallSphere.scale
            newNode.position = planeHitTestPosition
            self.sceneView?.scene.rootNode.addChildNode(newNode)
            
            let newLineNode = self.lineBetweenNodeA(nodeA: latestNode!, nodeB: newNode)
            self.sceneView?.scene.rootNode.addChildNode(newLineNode)
            
            if selectedType == .room {
                let newWallNode = self.planeBetweenNodeA(nodeA: latestNode!, nodeB: newNode)
                self.sceneView?.scene.rootNode.addChildNode(newWallNode)
            }
            
            let distance = distanceBetweenPoints(A: latestNode!.position, B: newNode.position)
            let distanceString = String(format: "%.2f", distance) + " m"
            //print("distance: " + distanceString)
            
            let text = SCNText (string: distanceString, extrusionDepth: 0.1)
            text.font = UIFont.systemFont(ofSize: 20)
            text.firstMaterial?.diffuse.contents = coolColor
            text.alignmentMode  = kCAAlignmentCenter
            text.truncationMode = kCATruncationMiddle
            text.firstMaterial?.isDoubleSided = true
            
            let textNode = SCNNode(geometry: text)
            textNode.scale = SCNVector3(1/500.0, 1/500.0, 1/500.0)
            textNode.eulerAngles = SCNVector3Make(0, .pi, 0)
            
            let textWrapNode = SCNNode()
            textWrapNode.addChildNode(textNode)
            textWrapNode.position = planeHitTestPosition
            
            let constraint = SCNLookAtConstraint(target: cameraNode)
            constraint.isGimbalLockEnabled = true
            textWrapNode.constraints = [constraint]
            self.sceneView?.scene.rootNode.addChildNode(textWrapNode)
            
            //
            nodesGroup?.allSphereNodes.append(newNode)
            nodesGroup?.allLineNodes.append(newLineNode)
            nodesGroup?.allDistanceNodes.append((textWrapNode, distanceString))
            
            latestNode = newNode
            
            //
            //if selectedType == .door || selectedType == .window {
            //    closeNodes(shouldClose: false)
            //}
        }
    }
    
    private func moveShapeNode(toPosition planeHitTestPosition: SCNVector3) {
        currentNode!.position = planeHitTestPosition
        
        if latestNode != nil {
            currentLineNode?.removeFromParentNode()
            currentLineNode = lineBetweenNodeA(nodeA: latestNode!, nodeB: currentNode!);
            self.sceneView?.scene.rootNode.addChildNode(currentLineNode!)
        }
    }
    
    private func positionFromTransform(_ transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
    }
    
    private func lineBetweenNodeA(nodeA: SCNNode, nodeB: SCNNode) -> SCNNode {
        let vector1 = nodeA.position
        let vector2 = nodeB.position
        
        let node:SCNNode = lineBetweenVectors(from: vector1, to: vector2, radius: 0.002, color: coolColor) //cylinder
        return node
    }
    
    private func planeBetweenNodeA(nodeA: SCNNode, nodeB: SCNNode) -> SCNNode {
        let vector1 = nodeA.position
        let vector2 = nodeB.position
        
        let node:SCNNode = planeBetweenVectors(from: vector1, to: vector2, color: coolColor) //cylinder
        return node
    }
    
    private func createPlaneNode(forAnchor planeAnchor: ARPlaneAnchor, ofOpacity
        opacity: CGFloat = 1.0) -> (plane: SCNPlane, node: SCNNode) {
        
        // Create a SceneKit plane to visualize the plane anchor using its position and extent.
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        
        let planeMaterial = SCNMaterial()
        planeMaterial.diffuse.contents = UIImage(named: "art.scnassets/mesh.png")
        
        plane.firstMaterial = planeMaterial
        
        let planeNode = SCNNode(geometry: plane)
        
        planeNode.geometry?.firstMaterial?.diffuse.wrapS = SCNWrapMode.repeat
        planeNode.geometry?.firstMaterial?.diffuse.wrapT = SCNWrapMode.repeat

        //let body = SCNPhysicsBody(type: SCNPhysicsBodyType.static, shape: SCNPhysicsShape(geometry: plane, options: nil))
//        SCNPhysicsBody * body =  [ SCNPhysicsBody bodyWithType : SCNPhysicsBodyTypeKinematic shape : [ SCNPhysicsShape shapeWithGeometry : ramp . geometry options : nil ] ] ;
       // planeNode.physicsBody = body
        
        planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z + 0)
        
        //Converting ARKit angles to Scenekit Angles... 3d to 2d..
        planeNode.eulerAngles.x = -.pi / 2
        planeNode.opacity = opacity
        
        return (plane, planeNode)
    }
    
    private func updatePlaneNode(forAnchor planeAnchor: ARPlaneAnchor, node: SCNNode) -> SCNPlane? {
        // Update plane (center, extent) added to node in didAdd
        
        guard let planeNode = node.childNodes.first else { return nil }
        
        guard let plane = planeNode.geometry as? SCNPlane else { return nil }
        
        // Plane estimation may shift the center of a plane relative to its anchor's transform.
        planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z + 0)

        /*
         Plane estimation may extend the size of the plane, or combine previously detected
         planes into a larger one. In the latter case, `ARSCNView` automatically deletes the
         corresponding node for one plane, then calls this method to update the size of
         the remaining plane.
         */
        plane.width = CGFloat(planeAnchor.extent.x)
        plane.height = CGFloat(planeAnchor.extent.z)
        
        return plane
    }
    
    private func updateSessionInfoLabel(for frame: ARFrame, trackingState: ARCamera.TrackingState) {
        // Update the UI to provide feedback on the state of the AR experience.
        let message: String
        let opacity: CGFloat
        
        switch trackingState {
        case .normal where frame.anchors.isEmpty:
            // No planes detected; provide instructions for this app's AR interactions.
            message = "Move the device around to detect horizontal surfaces." + " (High)"
            opacity = PlaneOpacity.high
        case .normal:
            // No feedback needed when tracking is normal and planes are visible.
            message = "" + " (High)"
            opacity = PlaneOpacity.high
        case .notAvailable:
            message = "Tracking unavailable." + " (Low)"
            opacity = PlaneOpacity.low
        case .limited(.excessiveMotion):
            message = "Tracking limited - Move the device more slowly." + " (Low)"
            opacity = PlaneOpacity.low
        case .limited(.insufficientFeatures):
            message = "Tracking limited - Point the device at an area with visible surface detail, or improve lighting conditions." + " (Low)"
            opacity = PlaneOpacity.low
        case .limited(.initializing):
            message = "Initializing AR session." + " (High)"
            opacity = PlaneOpacity.high
        }
        
        statusLabel.text = message
        planeOpacity = opacity
    }
    
    private func showAlertDialog(message: String) {
        let dialogMessage = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
        dialogMessage.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(dialogMessage, animated: true, completion: nil)
    }
    
    private func changeAppState(state: AppState) {
        
        switch state {
       
        case .planeNotSaved:
            [drawPlanButton, closeNodesButton].forEach { $0?.isEnabled = false; $0?.alpha = ButtonsOpacity.low }
            isPlaneDetected = false
            currentNode?.isHidden = true
            savedPlane = nil
            resetAreaVariables()
            break;
        case .planeSaved:
            print("plane saved")
            [drawPlanButton, closeNodesButton].forEach { $0?.isEnabled = true; $0?.alpha = 1.0 }
            isPlaneDetected = true
            currentNode?.isHidden = false
            
            break;
        case .lessOrNoSpheres:
            //TODO: Not handled
//            [drawPlanButton, closeNodesButton].forEach { $0?.isEnabled = false; $0?.alpha = ButtonsOpacity.low }
            break;
        case .minimumSpheresAdded:
            //TODO: Not handled
//            [drawPlanButton, closeNodesButton].forEach { $0?.isEnabled = true; $0?.alpha = 1.0 }
            break;
        }
        
    }
    
    private func createNodesGroup(ofType type: NodesGroupType) {
        self.nodesGroup = NodesGroup(groupType: type)
        self.nodesGroups.append(self.nodesGroup!)
    }
    
    private func closeNodes(shouldClose: Bool = true) {
        
        if shouldClose {
            guard let p = startNode?.position else { return }
            addShapeNode(atPosition: p)
        }
        
        self.startNode = nil
        self.latestNode = nil
        showAlertDialog(message: "Done adding \(selectedType)")
        
        currentLineNode?.removeFromParentNode()
        currentTextNode?.removeFromParentNode()
    }
    
    private func resetAreaVariables() {
        
        maxX = -CGFloat.greatestFiniteMagnitude
        minX = CGFloat.greatestFiniteMagnitude
        maxY = -CGFloat.greatestFiniteMagnitude
        minY = CGFloat.greatestFiniteMagnitude
    }
    
    //Actions
    @IBAction func clearPressed(_ sender: UIButton) {
        print(#function)
        
        //Remove UI
        nodesGroups.forEach({$0.allLineNodes.forEach({$0.removeFromParentNode()})})
        nodesGroups.forEach({$0.allDistanceNodes.forEach({$0.node.removeFromParentNode()})})
        nodesGroups.forEach({$0.allSphereNodes.forEach({$0.removeFromParentNode()})})
        
        //Reset Nodes Group
        nodesGroups.removeAll()
        
        //Reset Nodes
        self.startNode = nil
        self.latestNode = nil
        
        //Reset Current Nodes
        currentLineNode?.removeFromParentNode()
        currentTextNode?.removeFromParentNode()
        
        //App State
        changeAppState(state: AppState.planeNotSaved)
        
        //Reset state
        selectedType = .room
        
        //Alert
        showAlertDialog(message: "All Nodes and Saved Planes Cleared")
    }
    
    @IBAction func closeNodesPressed(_ sender: UIButton) {
        closeNodes()
    }
    
    @IBAction func chooseNodeType(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Choose Type", message: "Select type of room object", preferredStyle: UIAlertControllerStyle.alert)
        
        let room = UIAlertAction(title: "Room", style: .default) { (action) in self.selectedType = .room }
        let door = UIAlertAction(title: "Door", style: .default) { (action) in self.selectedType = .door }
        let window = UIAlertAction(title: "Window", style: .default) { (action) in self.selectedType = .window }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive)
        
        alertController.addAction(room)
        alertController.addAction(door)
        alertController.addAction(window)
        alertController.addAction(cancel)
        
        present(alertController, animated: true)
    }
    
}


extension ViewController: ARSCNViewDelegate, ARSessionDelegate {
    //Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        return SCNNode()
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        print(#function)
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        let result = createPlaneNode(forAnchor: planeAnchor, ofOpacity: planeOpacity)
        node.addChildNode(result.node)
        
        allPlanes[planeAnchor] = (result.plane, result.node)
        
        print("AllPlanes count: \(allPlanes.count)")
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        guard let planeAnchor = anchor as?  ARPlaneAnchor else { return }
        guard let plane = updatePlaneNode(forAnchor: planeAnchor, node: node) else { return }
        
        allPlanes[planeAnchor] = (plane, node)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        print(#function)
        
        guard let planeAnchor = anchor as?  ARPlaneAnchor else { return }
        
        allPlanes.removeValue(forKey: planeAnchor)
        
        print("AllPlanes count: \(allPlanes.count)")
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        DispatchQueue.main.async {
            if let camera = self.sceneView.session.currentFrame?.camera {
                let cameraPos = self.positionFromTransform(camera.transform)
                self.cameraNode?.position = cameraPos
                //self.childCameraNode?.position = SCNVector3(x: 0, y:0, z:-1)

                if self.isPlaneDetected! {
                    self.hitTest(at: self.sceneViewCenter, hitTestCase: .drawFloatingSphere);
                }
            }
        }

    }
    
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        if let frame = session.currentFrame {
            updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)
        }
    }
    
    func session(_ session: ARSession, didRemove anchors: [ARAnchor]) {
        if let frame = session.currentFrame {
            updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)
        }
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        updateSessionInfoLabel(for: session.currentFrame!, trackingState: camera.trackingState)
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        statusLabel.text = "Session Failed " + error.localizedDescription
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        statusLabel.text = "Session  Interrupted."
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        statusLabel.text = "Session Interruption Ended."
    }
    
}


extension ViewController: BlueprintProtocol {
    
    func totalNodes() -> [NodesGroup] {
        return nodesGroups
    }
    
    func totalArea() -> CGRect {
        return CGRect(x: minX, y: minY, width: maxX-minX, height: maxY-minY)
    }

}
