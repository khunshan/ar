//
//  AppConstants.swift
//  tinAR
//
//  Created by Khunshan Ahmad on 1/8/18.
//  Copyright © 2018 Khunshan Ahmad. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

struct PlaneOpacity {
    
    static let high: CGFloat = 0.22
    static let low: CGFloat = 0.10
}

struct ButtonsOpacity {
    static let low: CGFloat = 0.3
}

enum HitTestCase {
    case savePlane
    case drawSphere
    case drawFloatingSphere
}

enum AppState {
    case planeSaved
    case planeNotSaved
    case lessOrNoSpheres
    case minimumSpheresAdded
}

enum NodesGroupType {
    case room
    case door
    case window
}

//Some utility functions
func magnitude(ofVector vector: SCNVector3) -> CGFloat {
    let magnitude = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
    return CGFloat(magnitude)
}

func magnitude(ofPoint point: CGPoint) -> CGFloat {
    let magnitude = sqrt(point.x * point.x + point.y * point.y)
    return CGFloat(magnitude)
}

func distanceBetweenPoints(A: SCNVector3, B: SCNVector3) -> CGFloat {
    let distanceVector = SCNVector3Make(A.x - B.x, A.y - B.y, A.z - B.z)
    let distance = magnitude(ofVector: distanceVector)
    
    return distance
}

func normalizeVector(_ iv: SCNVector3) -> SCNVector3 {
    let length = sqrt(iv.x * iv.x + iv.y * iv.y + iv.z * iv.z)
    if length == 0 {
        return SCNVector3(0.0, 0.0, 0.0)
    }
    
    return SCNVector3( iv.x / length, iv.y / length, iv.z / length)
    
}

func cosAngleBetweenVectors (vector1: SCNVector3, vector2: SCNVector3) -> CGFloat {
    let dotProduct = dotProductBetweenVectors(vector1: vector1, vector2: vector2)
    let mag1 = magnitude(ofVector: vector1)
    let mag2 = magnitude(ofVector: vector2)
    
    let cosAngle = dotProduct/(mag1 * mag2)
    
    return cosAngle
}

func dotProductBetweenVectors (vector1: SCNVector3, vector2: SCNVector3) -> CGFloat{
    let product = vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z
    
    return CGFloat(product)
}

func lineBetweenVectors(from startPoint: SCNVector3,
                        to endPoint: SCNVector3,
                        radius: CGFloat,
                        color: UIColor) -> SCNNode {
    let node = SCNNode()
    
    let l = distanceBetweenPoints(A: endPoint, B: startPoint)
    
    if l == 0.0 {
        // two points together.
        let sphere = SCNSphere(radius: radius)
        sphere.firstMaterial?.diffuse.contents = color
        node.geometry = sphere
        node.position = startPoint
        return node
        
    }
    
    let cyl = SCNCylinder(radius: radius, height: l)
    cyl.firstMaterial?.diffuse.contents = color
    
    node.geometry = cyl
    
    //original vector of cylinder above 0,0,0
    let ov = SCNVector3(0, l/2,0)
    //target vector, in new coordination
    let nv = SCNVector3((endPoint.x - startPoint.x)/2.0, (endPoint.y - startPoint.y)/2.0,
                        (endPoint.z-startPoint.z)/2.0)
    
    // axis between two vector
    let av = SCNVector3( (ov.x + nv.x)/2.0, (ov.y+nv.y)/2.0, (ov.z+nv.z)/2.0)
    
    //normalized axis vector
    let av_normalized = normalizeVector(av)
    let q0 = Float(0.0) //cos(angel/2), angle is always 180 or M_PI
    let q1 = Float(av_normalized.x) // x' * sin(angle/2)
    let q2 = Float(av_normalized.y) // y' * sin(angle/2)
    let q3 = Float(av_normalized.z) // z' * sin(angle/2)
    
    let r_m11 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3
    let r_m12 = 2 * q1 * q2 + 2 * q0 * q3
    let r_m13 = 2 * q1 * q3 - 2 * q0 * q2
    let r_m21 = 2 * q1 * q2 - 2 * q0 * q3
    let r_m22 = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3
    let r_m23 = 2 * q2 * q3 + 2 * q0 * q1
    let r_m31 = 2 * q1 * q3 + 2 * q0 * q2
    let r_m32 = 2 * q2 * q3 - 2 * q0 * q1
    let r_m33 = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3
    
    node.transform.m11 = r_m11
    node.transform.m12 = r_m12
    node.transform.m13 = r_m13
    node.transform.m14 = 0.0
    
    node.transform.m21 = r_m21
    node.transform.m22 = r_m22
    node.transform.m23 = r_m23
    node.transform.m24 = 0.0
    
    node.transform.m31 = r_m31
    node.transform.m32 = r_m32
    node.transform.m33 = r_m33
    node.transform.m34 = 0.0
    
    node.transform.m41 = (startPoint.x + endPoint.x) / 2.0
    node.transform.m42 = (startPoint.y + endPoint.y) / 2.0
    node.transform.m43 = (startPoint.z + endPoint.z) / 2.0
    node.transform.m44 = 1.0
    return node
}

func planeBetweenVectors(from startPoint: SCNVector3,
                        to endPoint: SCNVector3,
                        color: UIColor) -> SCNNode {
    let width = distanceBetweenPoints(A: startPoint, B: endPoint)
    let height = CGFloat(10)
    
    // Create a SceneKit plane to visualize the plane anchor using its position and extent.
    let plane = SCNPlane(width: width, height: height)
    
    let planeNode = SCNNode(geometry: plane)
    
    let body = SCNPhysicsBody(type: SCNPhysicsBodyType.static, shape: SCNPhysicsShape(geometry: plane, options: nil))
    planeNode.physicsBody = body
    
    planeNode.simdPosition = float3(startPoint.x + (endPoint.x - startPoint.x) / 2, startPoint.y + (endPoint.y - startPoint.y) / 2 + 1, startPoint.z + (endPoint.z - startPoint.z) / 2)
    
    
    //Converting ARKit angles to Scenekit Angles... 3d to 2d..
    planeNode.eulerAngles.x = -.pi// / 2
    
    let deltaX = endPoint.x - startPoint.x
    let deltaZ = endPoint.z - startPoint.z
    
    var angle = atan(deltaX/deltaZ)
    angle -= .pi / 2
    
    planeNode.eulerAngles.y = planeNode.eulerAngles.y + Float(angle)
    
    planeNode.opacity = 0
    
    return planeNode
}

